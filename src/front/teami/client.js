console.log("client script");

// Create WebSocket connection.
const socket = io("ws://localhost:1337");

// Connection opened
socket.on("connect", function (event) {
    console.log("Socket Connected");
});

socket.on(JoinMessage.type, function (message) {
    console.log("Join Message received ")
    console.dir(message);
    createjs.Ticker.framerate = 60;
    createjs.Ticker.addEventListener("tick", handleTick);
});

socket.on(MoveBallMessage.type, function (message) {
    ball.x = message.x;
    ball.y = message.y
});


var match;
var currentPlayer;
const getMatches = function () {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://localhost:3000/matches/sdfsdfsdf', true);
    xhr.onreadystatechange = function (event) {
        if (this.readyState === XMLHttpRequest.DONE) {
            let response = JSON.parse(this.response);
            console.log(response[0].id);
        }
    };
    xhr.send();
}

getMatches();

const createMatch = function () {
    let xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://localhost:3000/matches', true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function (event) {
        if (this.readyState === XMLHttpRequest.DONE) {
            match = JSON.parse(this.response);
            console.dir(match);
            currentPlayer = new Player("sdfsdf", 0, 0, 0, 0, "damoebius")
            socket.emit(JoinRequest.type, new JoinRequest(match.id, currentPlayer));
        }
    };
    xhr.send(JSON.stringify({
        gameplayType: 'typedunexhr',
        balls: [new Ball("sdfefefd", 10, 10, 10, "", {})]
    }));
}

createMatch();


var stage = new createjs.Stage("demoCanvas");

var paddle = new createjs.Shape();
paddle.graphics.beginFill("#ff0000").drawRect(-10, -25, 20, 50);



var ball = new createjs.Shape();
ball.graphics.beginFill("#00FF00").drawCircle(-5, -5, 10);

ball.x = ball.y = 50;


stage.addChild(ball);
stage.addChild(paddle);

var ballSpeed = 4.0;

var ballXVector = 1;
var ballYVector = 1;

function xHitTest(nextPos) {
    var result = false;
    if (ball.x + 5 < paddle.x - 10
        && nextPos + 5 >= paddle.x - 10
        && (ball.y + 5 > paddle.y - 25 && ball.y - 5 < paddle.y + 25)
    ) {
        result = true;
    }

    if (ball.x + 5 > paddle.x + 10
        && nextPos <= paddle.x + 10
        && (ball.y > paddle.y - 25 && ball.y < paddle.y + 25)
    ) {
        result = true;
    }
    return result;
}

function moveBall() {

    var nextYPosition = ball.y + (ballSpeed * ballYVector);
    var nextXPosition = ball.x + (ballSpeed * ballXVector);

    if (nextYPosition > stage.canvas.height
        || nextYPosition <= 0
    ) {
        ballYVector *= -1;
    }

    //ball.y = nextYPosition;

    if (nextXPosition > stage.canvas.width
        || nextXPosition <= 0
        || xHitTest(nextXPosition)
    ) {
        ballXVector *= -1;
    }

    //ball.x = nextXPosition;

    socket.emit(MoveBallRequest.type, new MoveBallRequest(match.balls[0],nextXPosition,nextYPosition,0))

}

function handleTick(event) {
    moveBall();
    stage.update();
}


stage.addEventListener('stagemousemove', function (event) {
    paddle.x = event.stageX;
    paddle.y = event.stageY;
})
