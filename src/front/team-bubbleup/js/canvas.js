var stage = new createjs.Stage("canvas");

var canvas = document.getElementById("canvas");

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var paddle = new createjs.Bitmap("img/bob.png");


var ball = new createjs.Shape();
ball.graphics.beginFill("#00FF00").drawCircle(-5,-5,10);

ball.x = ball.y = 50;


stage.addChild(ball);
stage.addChild(paddle);

var ballSpeed = 4.0;

var ballXVector = 1;
var ballYVector = 1;

function xHitTest(nextPos){
    var result = false;
    if(ball.x +5 < paddle.x - 10
    && nextPos+5 >= paddle.x-10
    && (ball.y+5 > paddle.y -25 && ball.y-5 < paddle.y +25)
    ){
        result = true;
    }

    if(ball.x+5 > paddle.x + 10
    && nextPos <= paddle.x+10
    && (ball.y > paddle.y -25 && ball.y < paddle.y +25)
    ){
        result = true;
    }
    return result;
}

function moveBall(){

    var nextYPosition = ball.y + (ballSpeed*ballYVector);
    var nextXPosition = ball.x + (ballSpeed*ballXVector);

    if(nextYPosition > stage.canvas.height
    || nextYPosition <= 0
    ){
        ballYVector *= -1;
    }

    ball.y =  nextYPosition;

    if(nextXPosition > stage.canvas.width
    || nextXPosition <= 0
    || xHitTest(nextXPosition)
    ){
        ballXVector *= -1;
    }

    ball.x =  nextXPosition;

}

function handleTick(event) {
    moveBall();
    stage.update();
}

createjs.Ticker.framerate  = 60;
createjs.Ticker.addEventListener("tick", handleTick);

stage.addEventListener('stagemousemove',function(event){
    paddle.x = event.stageX;
    paddle.y = event.stageY;
})
