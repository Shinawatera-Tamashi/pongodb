class Player {
    constructor(id,x,y,z,score,name){
        this.id = id;
        this.x = x;
        this.y = y;
        this.z = z;
        this.score = score;
        this.name = name;
    };
}

class Match {
    constructor(id,players,balls,gameplayType){
        this.id = id;
        this.players = players;
        this.startDate = null;
        this.balls = balls;
        this.gameplayType = gameplayType;
    };
}

class Ball {
    constructor(id,x,y,z,type,props){
        this.id = id;
        this.x = x;
        this.y = y;
        this.z = z;
        this.type = type;
        this.props = props;
    };
}

class JoinRequest{
    constructor(idMatch,player){
        this.idMatch = idMatch;
        this.player = player;
    }
    static get type() { return "joinRequest"; }
}

class JoinMessage{
    constructor(player){
        this.player = player;
    }
    static get type() { return "joinMessage"; }
}

class MoveBallRequest{
    constructor(idBall,x,y,z){
        this.idBall = idBall;
        this.x = x;
        this.y = y;
        this.z = z;
    }
    static get type() { return "moveBallRequest"; }
}

class MoveBallMessage{
    constructor(idBall,x,y,z){
        this.idBall = idBall;
        this.x = x;
        this.y = y;
        this.z = z;
    }
    static get type() { return "moveBallMessage"; }
}

class StartMatchRequest{
    constructor(idMatch){
        this.idMatch = idMatch;
    }
    static get type() { return "startMatchRequest"; }
}

class StartMatchMessage{
    constructor(idMatch){
        this.idMatch = idMatch;
    }
    static get type() { return "startMatchMessage"; }
}

class MovePlayerRequest{
    constructor(idPlayer,x,y,z){
        this.idPlayer = idPlayer;
        this.x = x;
        this.y = y;
        this.z = z;
    }
    static get type() { return "movePlayerRequest"; }
}

class MovePlayerMessage{
    constructor(idPlayer,x,y,z){
        this.idPlayer = idPlayer;
        this.x = x;
        this.y = y;
        this.z = z;
    }
    static get type() { return "movePlayerMessage"; }
}