class JoinRequest{
    constructor(idMatch,player){
        this.idMatch = idMatch;
        this.player = player;
    }
    static get type() { return "joinRequest"; }
}

class JoinMessage{
    constructor(player){
        this.player = player;
    }
    static get type() { return "joinMessage"; }
}

class MoveBallRequest{
    constructor(idBall,x,y,z){
        this.idBall = idBall;
        this.x = x;
        this.y = y;
        this.z = z;
    }
    static get type() { return "moveBallRequest"; }
}

class MoveBallMessage{
    constructor(idBall,x,y,z){
        this.idBall = idBall;
        this.x = x;
        this.y = y;
        this.z = z;
    }
    static get type() { return "moveBallMessage"; }
}

class StartMatchRequest{
    constructor(idMatch){
        this.idMatch = idMatch;
    }
    static get type() { return "startMatchRequest"; }
}

class StartMatchMessage{
    constructor(idMatch){
        this.idMatch = idMatch;
    }
    static get type() { return "startMatchMessage"; }
}

class MovePlayerRequest{
    constructor(idPlayer,x,y,z){
        this.idPlayer = idPlayer;
        this.x = x;
        this.y = y;
        this.z = z;
    }
    static get type() { return "movePlayerRequest"; }
}

class MovePlayerMessage{
    constructor(idPlayer,x,y,z){
        this.idPlayer = idPlayer;
        this.x = x;
        this.y = y;
        this.z = z;
    }
    static get type() { return "movePlayerMessage"; }
}

module.exports.MovePlayerRequest = MovePlayerRequest;
module.exports.MovePlayerMessage = MovePlayerMessage;
module.exports.JoinRequest = JoinRequest;
module.exports.JoinMessage = JoinMessage;
module.exports.MoveBallRequest = MoveBallRequest;
module.exports.MoveBallMessage = MoveBallMessage;
module.exports.StartMatchRequest = StartMatchRequest;
module.exports.StartMatchMessage = StartMatchMessage;