console.log('starting pongodb server');
const net = require('net');
const Server = require('socket.io');
const express = require('express');
const bodyParser = require('body-parser');
const services = require('./mock-services');
const cors = require('cors');
const message = require('./message');

class APIServer {

    constructor(){
        this.server = express();
        this.server.use(bodyParser.json());
        this.server.use(cors());

        this.server.get('/matches/:type', function (req, res) {
            let type = req.params.type;
            res.status(200).send(services.getMatchesByType(type));
        });

        this.server.post('/matches', function (req, res) {
            let result = services.addMatch(req.body.gameplayType,req.body.balls);
            res.status(200).send(result);
        });

        this.server.listen(3000, function () {
            console.log('APIServer listening on port 3000!');
        })
    }
}

class SocketServer {

    constructor(){
        // On crée un serveur de socket
        this.server = new Server(1337);
        this.server.on("connect",(socket)=>this.newConnectionHandler(socket));
    
        
    };

    newConnectionHandler(socket){
        console.log("client connected ");
        let ref = this;
        socket.on(message.JoinRequest.type,function(request){
            let game = services.addPlayerToMatch(request.idMatch,socket,request.player);
            ref.sendTo(game.sockets,message.JoinMessage.type,new message.JoinMessage(request.player));
        }); 
        socket.on(message.MoveBallRequest.type,function(request){
            let game = services.getGameBySocket(socket);
            ref.sendTo(game.sockets,message.MoveBallMessage.type,new message.MoveBallMessage(request.idBall,request.x,request.y,request.z));
        });
        socket.on(message.StartMatchRequest.type,function(request){
            let game = services.getGameBySocket(socket);
            ref.sendTo(game.sockets,message.StartMatchMessage.type,new message.StartMatchMessage(request.idMatch));
        }); 
        socket.on(message.MovePlayerRequest.type,function(request){
            let game = services.getGameBySocket(socket);
            ref.sendTo(game.sockets,message.MovePlayerMessage.type,new message.MovePlayerMessage(request.idPlayer,request.x,request.y,request.z));
        });    
    }

    sendTo(sockets, type, message){
        for(let i =0; i<sockets.length; i++){
            sockets[i].emit(type,message);
        }
    }

}



const socketServer = new SocketServer();
const apiServer = new APIServer();
